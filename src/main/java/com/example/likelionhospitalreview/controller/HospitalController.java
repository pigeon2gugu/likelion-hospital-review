package com.example.likelionhospitalreview.controller;

import com.example.likelionhospitalreview.domain.dto.HospitalResponse;
import com.example.likelionhospitalreview.service.HospitalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hospitals")
public class HospitalController {
    private final HospitalService hospitalService;

    public HospitalController(HospitalService hospitalService) {
        this.hospitalService = hospitalService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<HospitalResponse> getById(@PathVariable Long id){
        HospitalResponse hospitalResponse = hospitalService.getHospitalById(id);
        return ResponseEntity.ok().body(hospitalResponse);
    }
}
