package com.example.likelionhospitalreview.repository;

import com.example.likelionhospitalreview.domain.entity.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalRepository extends JpaRepository<Hospital,Long> {
}
