package com.example.likelionhospitalreview.service;

import com.example.likelionhospitalreview.domain.dto.HospitalResponse;
import com.example.likelionhospitalreview.domain.entity.Hospital;
import com.example.likelionhospitalreview.exception.ErrorCode;
import com.example.likelionhospitalreview.exception.HospitalException;
import com.example.likelionhospitalreview.repository.HospitalRepository;
import org.springframework.stereotype.Service;


@Service
public class HospitalService {
    private final HospitalRepository hospitalRepository;

    public HospitalService(HospitalRepository hospitalRepository) {
        this.hospitalRepository = hospitalRepository;
    }

    public HospitalResponse getHospitalById(Long id) {
        Hospital hospital = hospitalRepository.findById(id)
                .orElseThrow(() -> new HospitalException(ErrorCode.NOT_FOUND, "해당 id의 병원 정보를 찾을 수 없습니다."));
        return HospitalResponse.builder()
                .id(hospital.getId())
                .hospitalName(hospital.getHospitalName())
                .roadNameAddress(hospital.getRoadNameAddress())
                .build();
    }

}
