package com.example.likelionhospitalreview.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionManager {

    @ExceptionHandler(HospitalException.class)
    public ResponseEntity<?> hospitalReviewExceptionHandler(HospitalException e) {
        return ResponseEntity.status(e.getErrorCode().getStatus())
                .body(e.getMessage());
    }

}
