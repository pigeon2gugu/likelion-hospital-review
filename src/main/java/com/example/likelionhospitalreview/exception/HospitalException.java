package com.example.likelionhospitalreview.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class HospitalException extends RuntimeException{
    private ErrorCode errorCode;
    private String message;

}
