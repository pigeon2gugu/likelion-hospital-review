package com.example.likelionhospitalreview.controller;

import com.example.likelionhospitalreview.domain.dto.HospitalResponse;
import com.example.likelionhospitalreview.service.HospitalService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@WebMvcTest(HospitalController.class)
class HospitalControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    HospitalService hospitalService;



    @Test
    @DisplayName("id로 병원 정보 조회-GET /api/v1/hospitals/id ")
    void get_hospital_id() throws Exception {
        Long id = 1L;
        HospitalResponse hospitalResponse = HospitalResponse.builder()
                .id(id)
                .hospitalName("A병원")
                .roadNameAddress("서울시 서초구")
                .build();

        given(hospitalService.getHospitalById(id)).willReturn(hospitalResponse);

        mockMvc.perform(get("/api/v1/hospitals/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.hospitalName").exists())
                .andExpect(jsonPath("$.hospitalName").value("A병원"))
                .andExpect(jsonPath("$.roadNameAddress").value("서울시 서초구"))
                .andDo(print());
        verify(hospitalService).getHospitalById(id);

    }
}